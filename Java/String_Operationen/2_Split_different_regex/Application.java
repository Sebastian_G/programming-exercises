package com.uebung;

import java.util.Arrays;

/**
 * @author Sebastian
 *
 * @Links 
 * 	https://www.geeksforgeeks.org/split-string-java-examples/
 */

public class Application {

	public static void main(String[] args) {

		String str = "word1, word2 word3@word4?word5.word6"; 
		
        String[] arrOfStr = str.split("[, ?.@]+"); 
        
        // print str
        Arrays.asList(str).forEach(s -> System.out.println(s));
        
        // print arrOfStr
        Arrays.asList(arrOfStr).forEach(s -> System.out.println(s));
	}
}