package com.uebung;

import java.util.Arrays;

/**
 * @author Sebastian
 *
 * @Links 
 * 	https://www.geeksforgeeks.org/split-string-java-examples/
 */

public class Application {

	public static void main(String[] args) {

        // Split Strings
        // .split(regex, limit)
		
        String str = "this-is-a-test-string"; 
        String[] arrOfStr = str.split("-",2);   
        Arrays.asList(arrOfStr).forEach(p -> System.out.println(p));
        // this
        // is-a-test-string
        
        
        String[] arrOfStr2 = str.split("-"); 
        Arrays.asList(arrOfStr2).forEach(p -> System.out.println(p));
        // this
        // is
        // a
        // test
        // string
        
        
        //        Regex       Limit            Result
        //          -           1       "this-is-a-test-string"
        //          -           2       "this", "is-a-test-string"
        //          -           3       "this", "is", "a-test-string"
        //          -           4       "this", "is", "a", "test-string"
        //          -           5       "this", "is", "a", "test", "string"

	}
}

//for (String a : arrOfStr) 
//    System.out.println(a); 

// Arrays.asList(arrOfStr2).forEach(p -> System.out.println(p));