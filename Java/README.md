# Java


* Runnable
* Streams
    - split
    - collect
    - map
    - filter

* Lambdas
* Reflection

* Spring
- @-Notation
- Executor
    - ThreadPoolTaskExecutor

* Java-Funktionen

* String-Operationen

## Compile
	javac .\Application.java
	java Application