package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links https://javabeginners.de/Arrays_und_Verwandtes/Streams.php
 */
 
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			
		// Lists
		
		List<String> myList = Arrays.asList("p1", "p3", "p2", "p6", "p5", "e1", "e2", "e3");
		
		
		System.out.println("\nList: ");
		myList
			.stream()
			.forEach(str -> System.out.print(str + " "));
			//.forEach(p -> System.out.println("Element: " + p));
		
		System.out.println("\n\nFilter: ");
		myList
			.stream()
			.filter(s -> s.startsWith("p"))
			.forEach(str -> System.out.print(str + " "));
		
		
		System.out.println("\n\nMap: ");
		myList
			.stream()
			.filter(s -> s.startsWith("p"))
			.map(String::toUpperCase)
			.forEach(str -> System.out.print(str + " "));
		
		
		System.out.println("\n\nSorted: ");
		myList
			.stream()
			.filter(s -> s.startsWith("p"))
			.map(String::toUpperCase)
			.sorted() 
			.forEach(str -> System.out.print(str + " "));
	}
	
	

}

/*
Streams des Interface java.util.stream.Stream<T> , nicht zu verwechseln mit den Ein- und Ausgabe-Streams des 
Packages java.io , stellen Ströme von Referenzen dar, die es erlauben, verkettete Operationen auf diesen 
Referenzen nacheinander oder parallel auszuführen. Die Daten, die durch die Referenzen repräsentiert werden, 
werden durch den Stream selbst nicht verändert.
Das Interface und die von ihm abgeleiteten Interfaces stellen lediglich eine Vielzahl von Methoden bereit, 
die in zwei Hauptkategorien eingeteilt werden und meist Lambda Ausdrücke als Argumente übergeben bekommen:

-> intermediäre Operationen (intermediate operations) liefern wiederum einen Stream, der weiterverarbeitet 
werden kann (z.B. filter(), map(), distinct(), sorted(), etc.).

-> terminale Operationen (terminal operations) führen ihrerseits Operationen auf den Referenzen des Streams 
aus (forEach(), reduce(), toArray(), etc.). Sie können einen Wert liefern und beenden den Strom. Ist ein Strom 
einmal geschlossen, so können keine weiteren Operationen auf ihm ausgeführt werden.

*/