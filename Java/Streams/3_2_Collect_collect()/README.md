# Notes

Es gibt Kollektoren, die die Stream-Elemente in einer Collection ablegen.

In diesem Beispiel werden alle Strings, die mit einem Großbuchstaben beginnen, in einer Ergebnis-Collection aufgesammelt. Den Kollektor wurde mit der Factory-Methode Collectors.toList() erstellt. Dieser legt die Elemente in einem Set ab.


# Output

str1
str2
str3
str4
str5
str6
Number of elements: 6
