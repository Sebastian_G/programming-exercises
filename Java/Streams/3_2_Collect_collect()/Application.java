package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links 
 * 	http://www.angelikalanger.com/Articles/EffectiveJava/75.Java8.Fundamental-Stream-Operations/75.Java8.Fundamental-Stream-Operations.html
 * 	http://www.angelikalanger.com/Articles/EffectiveJava/76.Java8.Stream-Collectors/76.Java8.Stream-Collectors.html
 * 	https://www.baeldung.com/java-8-collectors
 */

public class Application {

	public static void main(String[] args) {

		// Stream-Kollektoren: collect()

		// ... using Collectors  
		List<String> streamSource = Arrays.asList("str1", "str2", "str3", "str4", "str5", "str6");

		List<String> resultList =
				streamSource.parallelStream()
					.filter(w->w.length() > 0)
					.filter(w->Character.isLowerCase(w.charAt(0)))
					.collect(Collectors.toList()); 

		resultList.forEach(res -> System.out.println(res));
		
		Long nElements = streamSource.stream().collect(Collectors.counting());
		System.out.println("Number of elements: " + nElements);
	}
}


/*
Die collect()-Methode dient dazu, die Elemente aus einem Stream aufzusammeln, z.B. in einer Collection. Die 
collect-Operation ist eine terminale Operation. Ihre Vielseitigkeit liegt in der Fülle von Kollektoren, die an sie 
übergeben werden können. Das heißt, im Wesentlichen verrichten die Kollektoren die Arbeit, nicht so sehr die 
collect()-Methode selbst. Prinzipiell kann man Kollektoren selber bauen, indem man das Collector-Interface implementiert. 
Es gibt aber bereits mehr als dreißig vordefinierten Kollektoren. Die Factory-Methoden für diese vordefinierten Kollektoren 
findet man in der Klasse Collectors. Im Folgenden wollen wir sie uns genauer ansehen.
 */