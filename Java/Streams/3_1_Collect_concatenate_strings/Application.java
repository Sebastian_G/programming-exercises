package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links 
 * 	http://www.angelikalanger.com/Articles/EffectiveJava/75.Java8.Fundamental-Stream-Operations/75.Java8.Fundamental-Stream-Operations.html
 * 	http://www.angelikalanger.com/Articles/EffectiveJava/76.Java8.Stream-Collectors/76.Java8.Stream-Collectors.html
 */

public class Application {

	public static void main(String[] args) {
			
		// Stream-Kollektoren: collect()
		
		// Put strings together (concatenate strings) ...
		
		// ... using Collectors  
		List<String> streamSource = Arrays.asList("str1", "str2", "str3", "str4", "str5", "str6");
		String resultString = streamSource.stream().collect(Collectors.joining());
		System.out.println("String: " + resultString);
		
		// ... using StringBuilder
		StringBuilder sb = new StringBuilder();
		streamSource.stream().forEach(s -> sb.append(s));
		resultString = sb.toString();
		System.out.println("String: " + resultString);
		
	}
}


/*
Die collect()-Methode dient dazu, die Elemente aus einem Stream aufzusammeln, z.B. in einer Collection. Die 
collect-Operation ist eine terminale Operation. Ihre Vielseitigkeit liegt in der Fülle von Kollektoren, die an sie 
übergeben werden können. Das heißt, im Wesentlichen verrichten die Kollektoren die Arbeit, nicht so sehr die 
collect()-Methode selbst. Prinzipiell kann man Kollektoren selber bauen, indem man das Collector-Interface implementiert. 
Es gibt aber bereits mehr als dreißig vordefinierten Kollektoren. Die Factory-Methoden für diese vordefinierten Kollektoren 
findet man in der Klasse Collectors. Im Folgenden wollen wir sie uns genauer ansehen.
*/