package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links https://stackoverflow.com/questions/10530353/convert-string-array-to-arraylist
 */

public class Application {

	public static void main(String[] args) {
			
		// Convert String-Array to ArrayList (List<String>)
		
		String[] ports = {"port_5", "port_2", "port_4", "port_3", "port_1", "port_6"};
		List<String> myList = new ArrayList<String>();
		
		// unsorted
		System.out.println("unsorted: ");
		myList = Arrays.stream(ports).collect(Collectors.toList());
		myList.forEach(str -> System.out.print(str + " "));
		
		// sorted		
		System.out.println("\n\nsorted: ");
		myList = Arrays.stream(ports).sorted().collect(Collectors.toList());
		myList.forEach(str -> System.out.print(str + " "));
		
		// asList()
		System.out.println("\n\nasList(): ");
		myList = Arrays.asList(ports);
		myList.forEach(str -> System.out.print(str + " "));
	}
}
