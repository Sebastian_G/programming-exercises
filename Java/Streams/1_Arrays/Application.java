package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links https://javabeginners.de/Arrays_und_Verwandtes/Streams.php
 */
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Arrays
		int[] nums = {1,2,3,4,5};
		Stream.of(nums).forEach(n -> System.out.println(n));
		Arrays.stream(nums).forEach(n -> System.out.println(n));
		
		String[] ports = {"p1", "p2", "p3", "p4"};
		Arrays.stream(ports).forEach(port -> System.out.println(port));
		
	}
}

/*
Streams des Interface java.util.stream.Stream<T> , nicht zu verwechseln mit den Ein- und Ausgabe-Streams des 
Packages java.io , stellen Ströme von Referenzen dar, die es erlauben, verkettete Operationen auf diesen 
Referenzen nacheinander oder parallel auszuführen. Die Daten, die durch die Referenzen repräsentiert werden, 
werden durch den Stream selbst nicht verändert.
Das Interface und die von ihm abgeleiteten Interfaces stellen lediglich eine Vielzahl von Methoden bereit, 
die in zwei Hauptkategorien eingeteilt werden und meist Lambda Ausdrücke als Argumente übergeben bekommen:

-> intermediäre Operationen (intermediate operations) liefern wiederum einen Stream, der weiterverarbeitet 
werden kann (z.B. filter(), map(), distinct(), sorted(), etc.).

-> terminale Operationen (terminal operations) führen ihrerseits Operationen auf den Referenzen des Streams 
aus (forEach(), reduce(), toArray(), etc.). Sie können einen Wert liefern und beenden den Strom. Ist ein Strom 
einmal geschlossen, so können keine weiteren Operationen auf ihm ausgeführt werden.

*/