package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Sebastian
 *
 * @Links 
 * 	https://www.mkyong.com/java8/java-8-streams-filter-examples/
 */

public class Application {

	public static void main(String[] args) {

		// Filters in Streams
		
		List<String> words = Arrays.asList("word1", "word2", "word3", "word4", "word5", "word6");
		
		
		System.out.println("equals(): ");
		List<String> filteredWords = words.stream()
						.filter(line -> !"word6".equals(line))
						.collect(Collectors.toList());
		
		filteredWords.forEach(s -> System.out.print(s + ", ")); // "word1, word2, word3, word4, word5,"
		
		
		
		System.out.println("\n\nfindAny() + orElse()");
		String word = words.stream()
						.filter(line -> "word6".equals(line))
						.findAny()
						.orElse("test");
		System.out.println("line == 'word6' -> " + word);	// "word6"
		
		
		
		String word2 = words.stream()
				.filter(line -> "word8".equals(line))
				.findAny()
				.orElse("nothing found");
		System.out.println("line == 'word8' -> " + word2); 	// "nothing found"
 
	}
}