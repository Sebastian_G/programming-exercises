package com.uebung;

/**
 * @author Sebastian
 *
 * @link: http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
 */
 
public class Application  {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Runnable runnable = 
				() -> { 
					for(int i = 0; i < 50; i++)
						System.out.println("Lambda Runnable running: " + i); 
				};
				
		Thread thread = new Thread(runnable);
		thread.start();
		for(int i = 0; i < 50; i++)
			System.out.println("Main Thread: " + i);
	}
}
