package com.uebung;

/**
 * @author Sebastian
 *
 * @link: http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
 */
 
public class Application {

	public static void main(String[] args){
		System.out.println(Thread.currentThread().getName());
		for(int i=0; i<10; i++) 
		{
			new Thread("" + i) 
			{
				public void run() 
				{
					System.out.println("Thread: " + getName() + " running");
				}
			}.start();
		}
	}
}
