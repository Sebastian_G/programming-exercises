//package com.uebung;

/**
 * @author Sebastian
 *
 * @link: https://docs.oracle.com/javase/tutorial/essential/concurrency/runthread.html
 */
 
public class Application implements Runnable {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		(new Thread(new Application())).start();
	}

	@Override
	public void run() {
		for(int i = 0; i < 20; i++)
			System.out.println(i);
	}

}
