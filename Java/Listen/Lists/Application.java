package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

/**
 * @author Sebastian
 *
 * @Links 
 * 	http://www.codeadventurer.de/?p=1751
 * 	https://crunchify.com/how-to-iterate-through-java-list-4-way-to-iterate-through-loop/
 */

public class Application {

	public static void main(String[] args) {

		ArrayList<String> einkaufsListe = new ArrayList<>();
		
		einkaufsListe.add("Ei");
		einkaufsListe.add("Schokolade");
		einkaufsListe.add("Zahnpasta");
		einkaufsListe.add("Banane");
		einkaufsListe.add("Tiefkühlpizza");
		
		
		// go through the loop with size() and get(i)
		for (int i = 0; i < einkaufsListe.size(); i++){
		    System.out.println(einkaufsListe.get(i));
		}
		
		
		// go through the loop with do/while()	
		ListIterator<String> artikelIterator = einkaufsListe.listIterator(0);

		do{
			System.out.println(artikelIterator.next());
		} while(artikelIterator.hasNext());
		
		
		// iterate via "iterator loop"
		Iterator<String> artikelIterator2 = einkaufsListe.iterator();
		while (artikelIterator2.hasNext()) {
			System.out.println(artikelIterator2.next());
		}
		
		
		// go through the loop with for()	
		for (ListIterator li = einkaufsListe.listIterator(0); li.hasNext();){
			System.out.println(li.next());
		}
		
		
		// go through the loop with "Advance For Loop"
		for(String str : einkaufsListe) {
			System.out.println(str);
		}
			
		
		// go through the loop with lambda
		einkaufsListe.forEach(article -> System.out.println(article));
	}
}