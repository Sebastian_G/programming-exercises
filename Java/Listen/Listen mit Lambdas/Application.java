package com.uebung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Sebastian
 *
 * @link: https://blog.codecentric.de/2013/10/java-8-erste-schritte-mit-lambdas-und-streams/
 */

public class Application {

	public static void main(String[] args){

		List<String> myList = Arrays.asList("element1","element2","element3");
		
		System.out.println("Extendet for loop:");
		for (String element : myList) {
			System.out.println(element);
		}

		System.out.println("Iterable Interface:");
		myList.forEach(new Consumer<String>() {
			public void accept(String element) {
				System.out.println(element);
			}
		});
		
		System.out.println("Lambda Expressions:");
		myList.forEach((String element) -> System.out.println(element));
		
		System.out.println("Lambda Expressions (implizite Ableitung des Typs:");
		myList.forEach(element -> System.out.println(element));
		
		
		
		// -------------------------------------------------------------------// 
		// http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_13_003.htm#mjf4178c97cb04d7bdd4be130334579516
		
		List<String> myList2 = new ArrayList<String>();
		myList2.add("str1");
		myList2.add("str2");
		myList2.add("str3");
		myList2.forEach(element -> System.out.println(element));
		
	}
}
