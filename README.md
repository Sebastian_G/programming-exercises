# Programming Exercises

------
Repository aktuallisieren:

    git add .
    git commit -m "commit"
    git push origin master

-------

## Java
[Java](/Java/README.md)

-----


## C++
[C++](/cpp/README.md)